var dokument = {
  titleOne: prompt("Введите Ваш заголовок:", "Title tuum"),
  bodyOne: prompt("Введите Ваше тело:", "Corpus tuum"),
  footerOne: prompt("Введите Ваш футер", "Footer tuum"),
  dateOne: prompt("Введите Вашу дату", 0),
  prilozhenie: {
    titleTwo: prompt("Введите Ваш вложенный заголовок:", "Title tuum"),
    bodyTwo: prompt("Введите Ваше второе тело:", "Corpus tuum"),
    footerTwo: prompt("Введите Ваш второй футер:", "Footer tuum"),
    dateTwo: prompt("Введите Вашу вторую дату:", 0)
  }
};
function showText() {
  document.write("<h1>Главный заголовок: " + dokument.titleOne + "</h1><br>");
  document.write("<p>Главное тело: " + dokument.bodyOne + "<br>");
  document.write("<p>Главный футер: " + dokument.footerOne + "<br>");
  document.write("<p>Главная дата: " + dokument.dateOne + "<br>");
  document.write("<hr><br>");
  document.write(
    "<h2>Вложенный заголовок: " + dokument.prilozhenie.titleTwo + "</h2><br>"
  );
  document.write("<p>Вложенное тело: " + dokument.prilozhenie.bodyTwo + "<br>");
  document.write(
    "<p>Вложенный футер: " + dokument.prilozhenie.footerTwo + "<br>"
  );
  document.write("<p>Вложенная дата: " + dokument.prilozhenie.dateTwo + "<br>");
  document.write("<hr><br>");
}
showText();
